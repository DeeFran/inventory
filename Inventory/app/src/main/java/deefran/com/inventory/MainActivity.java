package deefran.com.inventory;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get string array of locations from resources
        String[] locations = getResources().getStringArray(R.array.locations);
        MainMenuAdapter adapter = new MainMenuAdapter(this, R.id.menuName, locations);

        setContentView(R.layout.activity_main);

        extras = new Bundle();


        AdapterView.OnItemClickListener locationsItemClickListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> listView,
                                    View itemView,
                                    int position,
                                    long id) {
                switch (position) {
                    case 0:
                        Intent freezerIntent = new Intent(MainActivity.this, LocationCategoryActivity.class);
                        extras.putString("location", "Freezer");
                        extras.putInt("color", R.color.blue);
                        freezerIntent.putExtras(extras);
                        startActivity(freezerIntent);
                        break;

                    case 1:
                        Intent fridgeIntent = new Intent(MainActivity.this, LocationCategoryActivity.class);
                        extras.putString("location", "Fridge");
                        extras.putInt("color", R.color.crimson);
                        fridgeIntent.putExtras(extras);
                        startActivity(fridgeIntent);
                        break;
                    case 2:
                        Intent pantryIntent = new Intent(MainActivity.this, LocationCategoryActivity.class);
                        extras.putString("location", "Pantry");
                        extras.putInt("color", R.color.olive);
                        pantryIntent.putExtras(extras);
                        startActivity(pantryIntent);
                        break;
                    case 3:
                        Intent testSwipeIntent = new Intent(MainActivity.this, SwipeLayoutActivity.class);
                        startActivity(testSwipeIntent);
                }
            }
        };
        ListView listView = (ListView) findViewById(R.id.activity_main);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(locationsItemClickListener);
    }

    public void addItemActivity(View view) {
        Intent addItemIntent = new Intent(MainActivity.this, AddItemActivity.class);
        startActivity(addItemIntent);
    }

}
