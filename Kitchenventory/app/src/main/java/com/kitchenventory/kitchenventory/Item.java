package com.kitchenventory.kitchenventory;

/**
 * Created by Daniel on 1/2/2017.
 */

public class Item {

    private String oName;
    private String oDescription;
    private int oCount;
    private Location oLoc;

    public Item(String name, String description, int count, Location loc) {
        this.oName = name;
        this.oDescription = description;
        this.oCount = count;
        this.oLoc = loc;
    }

    public static final Item[] items = {
            new Item("Heinz Ketchup - 8oz", "8oz bottle of Heinz Ketchup", 1, Location.REFRIGERATOR),
            new Item("Kirkland Signature - 16fl.oz Soymilk", "16 fluid oz carton of Soymilk", 3, Location.REFRIGERATOR),
            new Item("Heinz Ketchup - 8oz", "8oz bottle of Heinz Ketchup", 1, Location.REFRIGERATOR),

    }

}

enum Location {
    REFRIGERATOR, PANTRY, FREEZER
}
