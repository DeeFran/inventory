package deefran.com.inventory;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;

import java.util.List;

/**
 * Created by Daniel on 2/20/2017.
 */

public class ItemCursorAdapter extends CursorAdapter {

    private LayoutInflater mInflater;

    private Activity myActivity;

    private ListView list;

    private SwipeLayout lastClicked;

    public ItemCursorAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, flags);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View temp = mInflater.inflate(R.layout.list_item_2, parent, false);

        myActivity = (Activity) temp.getContext();

        list = (ListView) myActivity.findViewById(R.id.location_category_list);

        LinearLayout mine = (LinearLayout) temp.findViewById(R.id.surface_parent);

        mine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = list.getPositionForView(v);
                list.performItemClick(v, position, list.getItemIdAtPosition(position));
            }
        });

        Button plusButton = (Button) temp.findViewById(R.id.plus_button);
        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = list.getPositionForView(v);
                list.performItemClick(v, position, list.getItemIdAtPosition(position));
            }
        });

        Button minusButton = (Button) temp.findViewById(R.id.minus_button);
        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = list.getPositionForView(v);
                list.performItemClick(v, position, list.getItemIdAtPosition(position));
            }
        });

        SwipeLayout sLayout = (SwipeLayout) temp;

        sLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {
                ListView myList = (ListView) layout.getParent();
                for (int i = 0; i < myList.getChildCount(); i++) {
                    SwipeLayout listItem = (SwipeLayout) myList.getChildAt(i);
                    if (listItem != layout) {
                        listItem.close();
                    }
                }
            }

            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onClose(SwipeLayout layout) {

            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });

        return temp;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView itemName = (TextView) view.findViewById(R.id.item_name_2);
        TextView itemQuantity = (TextView) view.findViewById(R.id.item_quantity_2);

        itemName.setText(cursor.getString(cursor.getColumnIndex("NAME")));
        itemQuantity.setText(cursor.getString(cursor.getColumnIndex("QUANTITY")));

    }

}
