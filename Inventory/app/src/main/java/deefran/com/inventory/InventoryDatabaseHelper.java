package deefran.com.inventory;

import android.content.ContentValues;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Daniel on 1/10/2017.
 */

public class InventoryDatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "inventory"; // name of the database
    private static final int DB_VERSION = 3; // the version of the database

    InventoryDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION); // call to constructor of SQLiteOpenHelper superclass, takes Context, db name, feature relating to cursors, db version
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateMyDatabase(db, 0, DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateMyDatabase(db, oldVersion, newVersion);
    }

    // Returns true if item passed validation and was inserted into database
    public static boolean insertItem(SQLiteDatabase db, String name, double quantity, String location) {
        if (!(name.equals("")) && quantity != 0) {
            ContentValues itemValues = new ContentValues();
            itemValues.put("NAME", name);
            itemValues.put("QUANTITY", quantity);
            itemValues.put("LOCATION", location);
            db.insert("ITEM", null, itemValues);
            return true;
        } else {
            return false;
        }
    }

    private void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 1) {
            db.execSQL("CREATE TABLE ITEM ("
                    + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "NAME TEXT, "
                    + "QUANTITY INTEGER, "
                    + "LOCATION TEXT);");
            insertItem(db, "bread loaf", 1, InventoryLocation.FREEZER);
            insertItem(db, "pint of chocolate ice cream", 2, InventoryLocation.FREEZER);
            insertItem(db, "bag of potstickers", 1, InventoryLocation.FREEZER);
            insertItem(db, "bag of tortilla chips", 1, InventoryLocation.PANTRY);
            insertItem(db, "egg", 5, InventoryLocation.FRIDGE);



        }
        if (oldVersion < 3) {
            // Upgrade code here
            insertItem(db, "salad dressing", 5, InventoryLocation.FRIDGE);
            insertItem(db, "turkey", 5, InventoryLocation.FRIDGE);
            insertItem(db, "chicken", 5, InventoryLocation.FRIDGE);
            insertItem(db, "spinach salad", 5, InventoryLocation.FRIDGE);
        }

    }

}
