package deefran.com.inventory;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;

/**
 * Created by Daniel on 1/27/2017.
 */
public class AddItemActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener {

    Item itemAddition;
    // TextView for quantity of item to be added
    TextView quantityText;
    // int for incrementing/decrementing quantity of item to be added
    int quantity;
    // Database and cursor variables for activity
    private SQLiteDatabase db;
    private Cursor cursor;
    // Spinner for the location dropdown
    private Spinner locDropdown;
    // ListView for displaying the list of items
    private ListView itemList;
    // Toast for displaying any Toast messages in Activity
    Toast myToast;
    // InventoryDatabaseHelper
    SQLiteOpenHelper inventoryDatabaseHelper = new InventoryDatabaseHelper(this);
    // CursorAdapter for itemlist
    private CursorAdapter listAdapter;
    // last clicked Swipe Layout in itemList
    SwipeLayout clickedItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        // Get and set location dropdown OnItemSelectedListener
        locDropdown = (Spinner) findViewById(R.id.loc_dropdown);
        locDropdown.setOnItemSelectedListener(this);

        // Set itemList ListView to the ListView created in activity_add_item.xml and set OnItemClickListener
        itemList = (ListView)findViewById(R.id.add_item_list);
        itemList.setOnItemClickListener(this);

        // Sets the soft keyboard to be visible when the activity is created and the first EditText element (@+id/add_item_text) has focus
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

    }

    // Increment quantity in add_item_quantity TextView ID
    public void addToQuantity(View view) {
        quantityText = (TextView) findViewById(R.id.add_item_quantity);
        checkText(quantityText, true);
        quantity = Integer.valueOf(quantityText.getText().toString());
        quantity++;
        quantityText.setText(String.valueOf(quantity));
    }

    // Decrement quantity in add_item_quantity TextView ID until it is at 0
    public void subFromQuantity(View view) {
        quantityText = (TextView) findViewById(R.id.add_item_quantity);
        checkText(quantityText, false);
        quantity = Integer.valueOf(quantityText.getText().toString());
        if (quantity > 0) {
            quantity--;
            quantityText.setText(String.valueOf(quantity));
        } else {
            // Logic to cancel a showing toast before starting a new one to prevent stacked Toast/time on Toast
            if (myToast != null) {
                myToast.cancel();
            }
            myToast = Toast.makeText(this, "Can't add negative amount of items",
                    Toast.LENGTH_SHORT);
            myToast.show();
        }
    }

    // Check the TextView to make sure it is not empty and set it's text to 0 if incrementing and 1 if decrementing
    private void checkText(TextView checkText, boolean add) {
        if (checkText.getText().toString().equals("")) {
            if (add) {
                checkText.setText("0");
            } else {
                checkText.setText("1");
            }
        }
    }

    // OnItemSelectedListener for locDropdown Spinner
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        showSelectedItems();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // Can you select nothing in the locDropdown Spinner?
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Resources cleanup, close cursor and database
        cursor.close();
        db.close();
    }

    // Called when the add_item_button is clicked
    public void addItem(View view) {
        new AddItemsTask().execute();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (myToast != null) {
            myToast.cancel();
        }
        // Display Toast with id value of the item clicked -- Add delete item or decrement/increment quantity here
        myToast = Toast.makeText(AddItemActivity.this, String.valueOf(id), Toast.LENGTH_SHORT);
        myToast.show();
    }

    private class AddItemsTask extends AsyncTask<Void, Void, Boolean> {

        String mName;
        double mQuantity;
        String mLoc;

        @Override
        protected void onPreExecute() {
            mLoc = locDropdown.getSelectedItem().toString();
            mQuantity = Double.valueOf(String.valueOf(((TextView) findViewById(R.id.add_item_quantity)).getText()));
            mName = String.valueOf(((TextView) findViewById(R.id.add_item_text)).getText());
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            if (!InventoryDatabaseHelper.insertItem(db, mName, mQuantity, mLoc)) {
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean inserted) {
            if (!inserted) {
                if (myToast != null) {
                    myToast.cancel();
                }
                myToast = Toast.makeText(AddItemActivity.this, "You have to add something..", Toast.LENGTH_SHORT);
                myToast.show();
            } else {
                showSelectedItems();
            }
        }

    }

    // Separated method for querying database for the items with the selected location of the locDropdown
    private void showSelectedItems() {
        new ShowItemsTask().execute();
    }

    // Separated AsyncTask for querying database for the items with the selected location of the locDropdown
    private class ShowItemsTask extends AsyncTask<Void, Void, Boolean> {

        String location;

        protected void onPreExecute() {
            location = String.valueOf(locDropdown.getSelectedItem());
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                SQLiteOpenHelper inventoryDatabaseHelper = new InventoryDatabaseHelper(AddItemActivity.this);
                db = inventoryDatabaseHelper.getReadableDatabase();
                cursor = db.query("ITEM", new String[]{"_id", "NAME", "QUANTITY", "LOCATION"}, "LOCATION = ? AND QUANTITY > 0", new String[] {location}, null, null, "_id DESC;");
                return true;
            } catch (SQLiteException e) {
                return false;
            }
        }

        protected void onPostExecute(Boolean successful) {
            if (!successful) {
                // Display a toast if database unavailable
                Toast toast = Toast.makeText(AddItemActivity.this, "Database unavailable", Toast.LENGTH_LONG);
                toast.show();
            } else {
                if (listAdapter == null) {
                    listAdapter = new ItemCursorAdapter(AddItemActivity.this, cursor, 0);
                } else {
                    listAdapter.changeCursor(cursor);
                }
            }
            itemList.setAdapter(listAdapter);
        }

    }

}
