package deefran.com.inventory;

/**
 * Created by Daniel on 1/9/2017.
 */
public class Item {

    private String name;
    private double quantity;
    private String location;
    private int id;

    private Item(String iName, double iQuantity, String loc) {
        this.name = iName;
        this.quantity = iQuantity;
        this.location = loc;
    }

    @Override
    public String toString() {
        return name;
    }

}
