package deefran.com.inventory;

import android.content.ContentValues;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;

/**
 * Created by Daniel on 1/9/2017.
 */
public class LocationCategoryActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {


    private final String EXTRA_ITEMNO = "Item number";

    private SQLiteDatabase db;
    private Cursor cursor;
    private String loc;
    private ListView itemList;
    private Intent intent;
    private ActionBar actionBar;

    private CursorAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        intent = getIntent();
        Bundle extras = intent.getExtras();
        actionBar = getSupportActionBar();
        actionBar.setTitle(extras.getString("location"));
        actionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, extras.getInt("color"))));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_category);

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.location_category_layout);
        setLayoutColor(extras.getString("location"), linearLayout);

        itemList = (ListView) findViewById(R.id.location_category_list);
        Intent intent = getIntent();
        loc = intent.getStringExtra("location");

        itemList.setOnItemClickListener(this);

        new ShowItemsTask().execute();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cursor.close();
        db.close();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        long viewId = view.getId();

        switch ((int) viewId) {
            case R.id.plus_button:
                new ChangeItemQuantityTask().execute(Long.valueOf(0), id);
                break;
            case R.id.minus_button:
                new ChangeItemQuantityTask().execute(Long.valueOf(1), id);
                break;
            case R.id.surface_parent:
                for (int i = 0; i < itemList.getChildCount(); i++) {
                    SwipeLayout listItem = (SwipeLayout) itemList.getChildAt(i);
                    listItem.close();
                }
                SwipeLayout swipeLayout = (SwipeLayout) view.getParent();
                swipeLayout.toggle();
                break;
        }

    }

    private class ShowItemsTask extends AsyncTask<Void, Void, Boolean> {

        String location;

        protected void onPreExecute() {
            location = loc;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                SQLiteOpenHelper inventoryDatabaseHelper = new InventoryDatabaseHelper(LocationCategoryActivity.this);
                db = inventoryDatabaseHelper.getReadableDatabase();
                cursor = db.query("ITEM", new String[]{"_id", "NAME", "QUANTITY", "LOCATION"}, "LOCATION = ? AND QUANTITY > ?", new String[] {location, "0"}, null, null, "_id DESC;");
                return true;
            } catch (SQLiteException e) {
                return false;
            }
        }

        protected void onPostExecute(Boolean successful) {
            if (!successful) {
                // Display a toast if database unavailable
                Toast toast = Toast.makeText(LocationCategoryActivity.this, "Database unavailable", Toast.LENGTH_LONG);
                toast.show();
            } else {
                if (listAdapter == null) {
                    listAdapter = new ItemCursorAdapter(LocationCategoryActivity.this,
                            cursor,
                            0);
                } else {
                    listAdapter.changeCursor(cursor);
                }
            }
            itemList.setAdapter(listAdapter);
        }

    }

    private class ChangeItemQuantityTask extends AsyncTask<Long, Void, Boolean> {

        String location;

        protected void onPreExecute() {
            location = loc;
        }

        @Override
        protected Boolean doInBackground(Long... params) {

            // First Long param - 0 = increment, 1 = decrement

            // Second Long param - _id parameter

            String id = String.valueOf(params[1]);
            Long option = params[0];

            try {
                SQLiteOpenHelper inventoryDatabaseHelper = new InventoryDatabaseHelper(LocationCategoryActivity.this);
                db = inventoryDatabaseHelper.getWritableDatabase();
                Cursor tempCursor = db.query("ITEM", new String[] {"_id", "QUANTITY"}, "_id = ?", new String[] {id}, null, null, null);
                tempCursor.moveToFirst();
                int quantityToChange = tempCursor.getInt(tempCursor.getColumnIndex("QUANTITY"));
                Log.e("COLUMN INDEX QUANTITY= ", String.valueOf(tempCursor.getColumnIndex("QUANTITY")));
                Log.e("_ID value", String.valueOf(tempCursor.getInt(tempCursor.getColumnIndex("_id"))));
                if (option == 0) {
                    quantityToChange++;
                } else if (option == 1 && quantityToChange > 0) {
                    quantityToChange--;
                }
                ContentValues contentValues = new ContentValues();
                contentValues.put("QUANTITY", quantityToChange);
                db.update("ITEM", contentValues, "_id = ?", new String[] {id});
                tempCursor.close();
                return true;
            } catch (SQLiteException e) {
                return false;
            }
        }

        protected void onPostExecute(Boolean successful) {
            if (!successful) {
                // Display a toast if database unavailable
                Toast toast = Toast.makeText(LocationCategoryActivity.this, "Database unavailable", Toast.LENGTH_LONG);
                toast.show();
            } else {
                new ShowItemsTask().execute();
            }
        }

    }




    private void setLayoutColor(String location, LinearLayout linearLayout) {
        switch (location) {
            case "Fridge":
                linearLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.light_crimson));
                break;
            case "Freezer":
                linearLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.light_blue));
                break;
            case "Pantry":
                linearLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.light_olive));
                break;
        }
    }

}
