package deefran.com.inventory;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.daimajia.swipe.SwipeLayout;


/**
 * Created by Daniel on 3/1/2017.
 */
public class SwipeLayoutActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.swipe_layout_test);

        SwipeLayout swipeLayout = (SwipeLayout) findViewById(R.id.example1);

        swipeLayout.getSurfaceView().setOnClickListener(this);

        //set show mode.
        swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);

//add drag edge.(If the BottomView has 'layout_gravity' attribute, this line is unnecessary)
        swipeLayout.addDrag(SwipeLayout.DragEdge.Left, findViewById(R.id.bottom_wrapper));

        swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {
            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });

    }


    @Override
    public void onClick(View v) {
        SwipeLayout mine =(SwipeLayout) v.getParent();
        mine.toggle();
    }
}
