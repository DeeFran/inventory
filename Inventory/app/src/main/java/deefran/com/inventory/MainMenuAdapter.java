package deefran.com.inventory;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by Daniel on 3/20/2017.
 */

public class MainMenuAdapter extends ArrayAdapter {

    public MainMenuAdapter(Context context, int resource, Object[] objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.main_activity_list_item, parent, false);
        }

        TextView listItem = (TextView) convertView.findViewById(R.id.menuName);
        listItem.setText(getItem(position).toString());
        listItem.setTextColor(Color.WHITE);

        listItem.setHeight(parent.getMeasuredHeight() / 4 - 3);

        switch (getItem(position).toString()) {
            case "Freezer":
                listItem.setBackgroundColor(getContext().getResources().getColor(R.color.blue));
                break;
            case "Fridge":
                listItem.setBackgroundColor(getContext().getResources().getColor(R.color.crimson));
                break;
            case "Pantry":
                listItem.setBackgroundColor(getContext().getResources().getColor(R.color.olive));
                break;
            case "Grocery List":
                listItem.setBackgroundColor(Color.CYAN);
                break;
        }

        return convertView;
    }

}
