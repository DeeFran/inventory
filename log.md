# 100 Days Of Code - Log

---

### Day 1: Sunday, January 1, 2017

**Today's Progress**: Researched and experimented with Google Barcode Reader/Scanner API for an app idea I have for keeping inventory of what's in your refrigerator/freezer/kitchen. See first day's commit on my Github.

**Thoughts** I've been taking an online class on Java and have not been doing too much actual Android development in quite some time. Hoping it does not take me too long to get back into it! This seems like a good start.
**Definitely exciting to see and experiment with what it will take to make an app idea reality. I should work on basic layout soon/next.

**Link(s) to work**
https://github.com/DeeFran74/100-days-of-code - first commit - Happy 2017

---

### Day 2: Monday, January 2, 2017

**Today's Progress**: Looked into structure of how I want the refrigerator/kitchen inventory app to be and started Android project for it. Researched multiple APIs that give back information from UPC codes, Amazon/AWS and MyFitnessPal
**seem like the most likely candidates.

**Thoughts** Starting and thinking about how to start really are the hardest parts of the process so far and after today, I'm thinking that I should start with something really simple and bare bones since this is a learning experience after all.

**Link(s) to work**
https://github.com/DeeFran74/100-days-of-code - second commit - added 'Kitchenventory' app folder (ridiculous name..I know)

### Day 3: Tuesday, January 3, 2017

**Today's Progress**: Reviewing Head First Android in order to get my Android basics back to par.  Not too much code work today, but there was lots of code review.

**Thoughts** Unsure if I want to count this day as a day of the 100 as I was doing a lot of code review and reading, but not as much actual coding. It's hard getting to a point where I feel comfortable simply writing code.
**Always sort of wondering if I'm structuring things correctly, but from what I have been told, simply writing code, right or wrong, is the best way to learn.  Updated the log a day late.

**Link(s) to work**
https://github.com/DeeFran74/100-days-of-code

