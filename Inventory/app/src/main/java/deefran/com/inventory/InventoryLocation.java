package deefran.com.inventory;

/**
 * Created by Daniel on 1/9/2017.
 */
public class InventoryLocation {
    public static final String FREEZER = "Freezer";
    public static final String FRIDGE = "Fridge";
    public static final String PANTRY = "Pantry";
}
